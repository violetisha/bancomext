'use strict';

const gulp = require('gulp'),
      newer = require('gulp-newer'),
      imagemin = require('gulp-imagemin'),
      sass = require('gulp-sass'),
      sourcemaps = require('gulp-sourcemaps'),
      autoprefixer = require('gulp-autoprefixer'),
      cssnano = require('gulp-cssnano'),
      rename = require('gulp-rename'),
      concat = require('gulp-concat'),
      uglify = require('gulp-uglify'),
      gutil = require('gulp-util'),
      lodash = require('lodash'),
      pug = require('gulp-pug'),
      plumber = require('gulp-plumber'),
      beep = require('beepbeep'),
      browsersync = require('browser-sync');


const folder = {
    src: 'src/',     // source files
    dist: 'dist/'   // distribution files
};

// Image processing
gulp.task('imageMin', function(){
    var out = folder.dist + 'img';
    return gulp.src(folder.src + 'img/**/*')
        .pipe(newer(out))
        .pipe(imagemin())
        .pipe(gulp.dest(out));
});

// Copy fonts
gulp.task('fonts', function(){
    var out = folder.dist + 'fonts/';
    return gulp.src([
            folder.src + 'fonts/**/*',
        ])
        .pipe(gulp.dest(out));
});

// Copy utils
gulp.task('utils', function(){
    return gulp.src([
            folder.src + '*.ico',
            folder.src + 'browserconfig.xml',
            folder.src + '*.png'
        ])
        .pipe(gulp.dest(folder.dist));
});

gulp.task('pug',function() {
 return gulp.src(folder.src + 'pug/*.pug')
 .pipe(plumber({
   errorHandler: function (error) {
     beep()
     gutil.log(gutil.colors.red('================='))
     gutil.log(gutil.colors.red('= PUG ERROR'))
     gutil.log(gutil.colors.red('================='))
     gutil.log(error.message)
     this.emit('end')
   }
 }))
 .pipe(pug({
    doctype: 'html',
    pretty: true
 }))
 .pipe(gulp.dest(folder.dist));
});


// Compile & minify SASS
gulp.task('css', function () {
    return gulp.src([
            folder.src + '/sass/styles.sass'
        ])
        .pipe(plumber({
          errorHandler: function (error) {
            beep()
            gutil.log(gutil.colors.red('================='))
            gutil.log(gutil.colors.red('= SASS ERROR'))
            gutil.log(gutil.colors.red('================='))
            gutil.log(error.message)
            this.emit('end')
          }
        }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 version']
        }))
        .pipe(gulp.dest(folder.dist + 'css/'))
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(cssnano({
            discardComments: {removeAllButFirst: true}
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(folder.dist + 'css/'));
});

// Concat and minify Javascripts
gulp.task('javascript', function(){
    var out = folder.dist + 'js/';

    // Keep in mind the order of the files.
    return gulp.src([
            folder.src + 'js/jquery-3.3.1.js',
            folder.src + 'js/script.js'
        ])
        .pipe(plumber({
          errorHandler: function (error) {
            beep()
            gutil.log(gutil.colors.red('================='))
            gutil.log(gutil.colors.red('= JS ERROR'))
            gutil.log(gutil.colors.red('================='))
            gutil.log(error.message)
            this.emit('end')
          }
        }))
        .pipe(sourcemaps.init())
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest(out))
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(uglify())
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(out));
});

// Live browser loading
gulp.task('browserSync', function() {
    browsersync.init({
        server: {
            baseDir: folder.dist
        }
    });
});

// Watch all changes
gulp.task('watch', function(){
    gulp.watch(folder.src + 'pug/**/*', ['pug', browsersync.reload]);
    gulp.watch(folder.src + 'img/**/*', ['imageMin', browsersync.reload]);
    gulp.watch(folder.src + 'fonts/**/*', ['fonts', browsersync.reload]);
    gulp.watch(folder.src + 'sass/**/*', ['css', browsersync.reload]);
    gulp.watch(folder.src + 'js/**/*', ['javascript', browsersync.reload]);
});

// Default task
gulp.task('default',
    ['utils', 'pug', 'imageMin', 'fonts', 'css', 'javascript', 'browserSync', 'watch']);
